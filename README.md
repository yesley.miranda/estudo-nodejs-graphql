# Estudo Nodejs Graphql

### Objetivo
 - Velocidade nas entregas
  

### Prós
 - 30% maior velocidade desenvolvimento, comparada com Nodejs Rest

### Contra
 - Curva de aprendizado



### Alguns desafios da vida real
1. Conexão com banco, como fica ?
    - Não muda nada, sendo aberto a possibilidade de uso de qualquer ORM (sequelize, mongosse,prisma,...)
2. Autenticação
    - Permissão de disponibilidade dos dados
    - Consigo separar ADMIN e COMUM ?
3. Permissão de acesso as rotas
    - Consigo separar ADMIN e COMUM ?
    
    
### Subir o banco mysql docker
`docker run --name banco-mysql -e MYSQL_ROOT_PASSWORD=senha -e MYSQL_DATABASE=banco -p 3306:3306 -d mysql:8.0.21`