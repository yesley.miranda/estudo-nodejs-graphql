import {gql} from 'apollo-server'

export default gql`

    extend type Query {
        usuario(nome: String): Usuario!
        usuarios: [Usuario!]!
        autenticacao(nome_usuario: String!, senha: String!): Autenticacao!
    }

    extend type Mutation {
        criarUsuario(
            nome: String!
            nome_usuario: String!
            senha: String!
        ) : Usuario!
    }

    type Usuario {
        id: Int
        nome: String
        nome_usuario: String
        senha: String
        removido: Boolean
        criado_em: String
        alterado_em: String
    }

    type Autenticacao {
        usuario: Usuario!
        token: String
        refreshToken: String
    }
`