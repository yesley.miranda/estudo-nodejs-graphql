import {gql} from 'apollo-server'

export default gql`
    type Query {
        ping: String
        _: String
    }

    type Mutation {
        _: String
    }
`