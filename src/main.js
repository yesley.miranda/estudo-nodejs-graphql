import {ApolloServer} from 'apollo-server'

import typeDefs from './type-defs'
import resolvers from './resolvers'

const server = new ApolloServer({typeDefs, resolvers})

server.listen()
    .then(() => console.log('Servidor rodando em http://localhost:4000'))
    .catch(e => console.error('Falha iniciar servidor', e))