import root from './root'
import usuarioResolver from './usuario'

export default [root, usuarioResolver]